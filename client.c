
#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <packedobjects/packedobjects.h>

#define XML_SCHEMA "customer.xsd"
#define MAXCHARS 256
#define NUM_CUSTOMERS 1

#define HOST_IP "127.0.0.1"
#define HOST_PORT 6969

int add_customer_data(xmlDocPtr doc)
{
  xmlNodePtr root_node = NULL, node = NULL, node_name = NULL, node_birthday = NULL, node_email = NULL, node_phone = NULL, node_address = NULL;
  char buffer[MAXCHARS];
  int id = 0, temp = 0;

  printf("\nThe purpose of this program is to register customer informations:\n");

  root_node = xmlNewNode(NULL, BAD_CAST "customers");
  xmlDocSetRootElement(doc, root_node);

  for (id = 1; id <= NUM_CUSTOMERS; id++) {
    // lets clear our buffer each time
    bzero(buffer, MAXCHARS);
    
    printf("\nCreating customer's number %d\n\n", id);
    node = xmlNewChild(root_node, NULL, BAD_CAST "customer", NULL);
   
    // convert integer to string
    sprintf(buffer, "%d", id);
    xmlNewChild(node, NULL, BAD_CAST "id", BAD_CAST buffer);

    //when the customer is created, he is automaticaly active
    xmlNewChild(node, NULL, BAD_CAST "active", BAD_CAST "true");
    
    // get the customer name
    node_name = xmlNewChild(node, NULL, BAD_CAST "name", NULL);

    // first name
    bzero(buffer, MAXCHARS);
    while (buffer[0] == '\0' || buffer[0] == '\n') {
    printf("Enter customer's given name: ");
    if (fgets(buffer, MAXCHARS, stdin) == NULL) {
      fprintf(stderr, "failed to read string\n");
      return (EXIT_FAILURE);
    }
    }
    strtok(buffer, "\n");
    xmlNewChild(node_name, NULL, BAD_CAST "givenName", BAD_CAST buffer);

    // middle name (optional)
    bzero(buffer, MAXCHARS);
    printf("Enter customer's middle name (if there is one): "); 
    fgets(buffer, MAXCHARS, stdin);
    if (buffer[0] != '\0' && buffer[0] != '\n') {
    strtok(buffer, "\n");
    xmlNewChild(node_name, NULL, BAD_CAST "middleName", BAD_CAST buffer);
    }

    // last name
    bzero(buffer, MAXCHARS);
    while (buffer[0] == '\0' || buffer[0] == '\n') {
    printf("Enter customer's last name: ");
    if (fgets(buffer, MAXCHARS, stdin) == NULL) {
      fprintf(stderr, "failed to read string\n");
      return (EXIT_FAILURE);
    }
    }
    strtok(buffer, "\n");
    xmlNewChild(node_name, NULL, BAD_CAST "lastName", BAD_CAST buffer);

    printf("\n");
    //get the customer genre
    bzero(buffer, MAXCHARS);
    while (buffer[0] != 'M' && buffer[0] != 'F') {
    printf("Enter customer's gender: \nM for male or F for female: ");
    if (fgets(buffer, MAXCHARS, stdin) == NULL) {
      fprintf(stderr, "failed to read string\n");
      return (EXIT_FAILURE);
    }
    }
    strtok(buffer, "\n");
    xmlNewChild(node, NULL, BAD_CAST "gender", BAD_CAST buffer);

    // get the customer date of birth
    node_birthday = xmlNewChild(node, NULL, BAD_CAST "birthday", NULL);
    printf("\nEnter customer's date of birth\n");

    // day
     temp = 0;
     while (temp < 1 || temp > 31) {
       printf("Day: ");
       scanf("%d", &temp);
     }
    sprintf(buffer, "%d", temp);
    xmlNewChild(node_birthday, NULL, BAD_CAST "day", BAD_CAST buffer);

    // month
    temp = 0;
    while (temp < 1 || temp > 12) {
      printf("Month: ");
      scanf("%d", &temp);
    }
    sprintf(buffer, "%d", temp);
    xmlNewChild(node_birthday, NULL, BAD_CAST "month", BAD_CAST buffer);

     // year
    temp = 0;
    while (temp < 1900 || temp > 2015) {
      printf("Year: ");
      scanf("%d", &temp);
    }
    sprintf(buffer, "%d", temp);
    xmlNewChild(node_birthday, NULL, BAD_CAST "year", BAD_CAST buffer);

  
    //get the customer email
    node_email = xmlNewChild(node, NULL, BAD_CAST "email", NULL);
    temp = 1;
    while (temp == 1) {

      //clean buffers
      bzero(buffer, MAXCHARS);
      fflush(stdin);
      __fpurge(stdin);

      while (buffer[0] == '\0' || buffer[0] == '\n') {
	printf("\nEnter customer's email:  ");
	if (fgets(buffer, MAXCHARS, stdin) == NULL) {
	  fprintf(stderr, "failed to read string\n");
	  return (EXIT_FAILURE);
	}
      }
      strtok(buffer, "\n");
      xmlNewChild(node_email, NULL, BAD_CAST "email", BAD_CAST buffer);
      do {
      	printf("\nInsert 1 to include another email or 0 to jump to the next step:  ");
      	scanf("%d", &temp);
      } while (temp != 0 && temp != 1);
    }

    //get the customer contact numbers
    node_phone = xmlNewChild(node, NULL, BAD_CAST "phone", NULL);
    temp = 1;
    while (temp == 1) {

      //clean buffers
      bzero(buffer, MAXCHARS);
      fflush(stdin);
      __fpurge(stdin);

      while (buffer[0] == '\0' || buffer[0] == '\n') {
	printf("\nEnter customer's contact number:  ");
	if (fgets(buffer, MAXCHARS, stdin) == NULL) {
	  fprintf(stderr, "failed to read string\n");
	  return (EXIT_FAILURE);
	}
      }
      strtok(buffer, "\n");
      xmlNewChild(node_phone, NULL, BAD_CAST "phone", BAD_CAST buffer);
      do {
	printf("\nInsert 1 to include another contact number or 0 to jump to the next step:  ");
      	scanf("%d", &temp);
      } while (temp != 0 && temp != 1);
    }       

    // get the customer address
    node_address = xmlNewChild(node, NULL, BAD_CAST "address", NULL);
    printf("\nEnter customer's address:\n");

    //clean buffers
      bzero(buffer, MAXCHARS);
      fflush(stdin);
      __fpurge(stdin);

    // First line
    bzero(buffer, MAXCHARS);
    while (buffer[0] == '\0' || buffer[0] == '\n') {
    printf("First line: ");
    if (fgets(buffer, MAXCHARS, stdin) == NULL) {
      fprintf(stderr, "failed to read string\n");
      return (EXIT_FAILURE);
    }
    }
    strtok(buffer, "\n");
    xmlNewChild(node_address, NULL, BAD_CAST "firstLine", BAD_CAST buffer);
     

    // Second line (optional)
    bzero(buffer, MAXCHARS);
    printf("Second line: "); 
    fgets(buffer, MAXCHARS, stdin);
    if (buffer[0] != '\0' && buffer[0] != '\n') {
    strtok(buffer, "\n");
    xmlNewChild(node_address, NULL, BAD_CAST "secondLine", BAD_CAST buffer);
    }

    // postcode
    bzero(buffer, MAXCHARS);
    while (buffer[0] == '\0' || buffer[0] == '\n') {
    printf("Postcode: ");
    if (fgets(buffer, MAXCHARS, stdin) == NULL) {
      fprintf(stderr, "failed to read string\n");
      return (EXIT_FAILURE);
    }
    }
    strtok(buffer, "\n");
    xmlNewChild(node_address, NULL, BAD_CAST "postcode", BAD_CAST buffer);

    // city
    bzero(buffer, MAXCHARS);
    while (buffer[0] == '\0' || buffer[0] == '\n') {
    printf("City: ");
    if (fgets(buffer, MAXCHARS, stdin) == NULL) {
      fprintf(stderr, "failed to read string\n");
      return (EXIT_FAILURE);
    }
    }
    strtok(buffer, "\n");
    xmlNewChild(node_address, NULL, BAD_CAST "city", BAD_CAST buffer);

    // country
    bzero(buffer, MAXCHARS);
    while (buffer[0] == '\0' || buffer[0] == '\n') {
    printf("Country: ");
    if (fgets(buffer, MAXCHARS, stdin) == NULL) {
      fprintf(stderr, "failed to read string\n");
      return (EXIT_FAILURE);
    }
    }
    strtok(buffer, "\n");
    xmlNewChild(node_address, NULL, BAD_CAST "country", BAD_CAST buffer);

    printf("\n------------------------------------------------------\n");
  } 
  
  return (EXIT_SUCCESS);
}

int main(int argc, char **argv)
{
  xmlDocPtr doc = NULL;
  packedobjectsContext *pc = NULL;
  char *pdu = NULL;
  ssize_t bytes_sent;
  int       sock;                                                 
  struct    sockaddr_in servaddr;

  // setup socket
  if ( (sock = socket(AF_INET, SOCK_STREAM, 0)) < 0 ) {
    fprintf(stderr, "Error creating socket.\n");
    exit(EXIT_FAILURE);
  }
  // setup addressing
  memset(&servaddr, 0, sizeof(servaddr));
  servaddr.sin_family      = AF_INET;
  servaddr.sin_addr.s_addr = inet_addr(HOST_IP);
  servaddr.sin_port        = htons(HOST_PORT); 
  
  // initialise packedobjects
  if ((pc = init_packedobjects(XML_SCHEMA, 0, 0)) == NULL) {
    printf("failed to initialise libpackedobjects");
    exit(1);
  }
  
  // create the data
  doc = xmlNewDoc(BAD_CAST "1.0");
  add_customer_data(doc);
  
  // encode the XML DOM
  pdu = packedobjects_encode(pc, doc);
  if (pc->bytes == -1) {
    printf("Failed to encode with error %d.\n", pc->encode_error);
  }

  // make network connection
  if (connect(sock, (struct sockaddr *) &servaddr, sizeof(servaddr)) < 0) {
    fprintf(stderr, "Error calling connect()\n");
    exit(EXIT_FAILURE);
  }
  
  // send the pdu across the network
  bytes_sent = send(sock, pdu, pc->bytes, 0);

  if (bytes_sent != pc->bytes) {
    fprintf(stderr, "Error calling send()\n");
    exit(EXIT_FAILURE);
  }

  if ( close(sock) < 0 ) {
    fprintf(stderr, "Error calling close()\n");
    exit(EXIT_FAILURE);
  }  
  
  // free the DOM
  xmlFreeDoc(doc);  
  
  // free memory created by packedobjects
  free_packedobjects(pc);
  
  xmlCleanupParser();
  return(0);
}
